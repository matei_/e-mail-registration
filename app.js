const express = require('express')
const emailRouter = require('./routers/email')
const handlebars = require('express-handlebars')

const app = express()
const port = 3000 // localhost

app.use(express.urlencoded())
app.use(emailRouter)
app.use(express.static(__dirname + '/public'));

app.set('view engine', 'hbs')
app.engine('hbs', handlebars({
    layoutsDir: __dirname + '/views/layouts',
    extname: 'hbs',
    defaultLayout: 'index'
}))

app.get('/', (req, res) => {
    res.render('main', {
        layout: 'index'
    })
})


app.listen(port, () => console.log(`Listening on port:`, port))