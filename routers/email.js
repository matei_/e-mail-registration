const express = require('express')
const router = new express.Router()
const application = require('../src/application')
const nodeMailer = require('nodemailer')

const arond = '@'


const transport = nodeMailer.createTransport({
    name: 'www.yourwebsite.com',
    host: 'mail.yourwebsite.com',
    port: 465,
    //secure: true,
    auth: {
        user: 'user',
        pass: 'pass'
    }
});

router.get('/email', (req, res) => {
    res.render('email', {
        layout: 'index'
    })
})

router.get('/email/register', (req, res) => {
    res.render('email/register', {
        layout: 'index'
    })
})

router.post('/email/:email', (req, res) => {
    const message = {
        from: 'Cerere e-mail personalizat <no-rply@emails.ro>',
        to: 'office@navodari.ro',
        subject: `Formular nou trimis pentru e-mail personalizat.`,
        html: application.formatHtmlBody(req.body, req.params.email)
    }

    res.render('email/' + req.params.email, {
        layout: 'index',
        submitted: true
    })
    
    transport.sendMail(message, (err, info) => {
        if(err) {
            console.log(err)
        } else {
            console.log(info)
        }
    })
})

module.exports = router
